# Image Processing Accelerating Multimedia 2D DCT Image Algorithm Using Intel IPP Integrated Performance Primitives Library

Program for 2D DCT (Discrete Cosine Transform) on image input without and with multimedia algorithms acceleration using optimized functions from Intel IPP library is in "dz3i4.c".

Additionally, Performance Profiling using Intel VTune Profiler was done for the program.

Project is realized in Microsoft Visual Studio and files of name "MASLab3*" are part of the project.

There are two additional files: "profile.txt" - performance profile of the program without IPP acceleration, "vremena.txt" - execution time of the program without (1st line: "DZ4") and with (2nd line: "DZ5") IPP acceleration.

Implemented in C.

My lab assignment in Multimedia Architecture and Systems, FER, Zagreb.

Task descriptions in "TaskSpecification.pdf".

Created: Jan 2022