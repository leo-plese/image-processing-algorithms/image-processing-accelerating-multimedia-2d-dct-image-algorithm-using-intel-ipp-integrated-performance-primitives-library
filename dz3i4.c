#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <stdlib.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <time.h>

// DZ3: komentirati sljedecu liniju (#define USE_IPP), DZ4: ostaviti sljedecu liniju
#define USE_IPP
#ifdef USE_IPP
#include "ipp.h"
#endif

#define IMG_DIM 512
#define BLOCK_DIM 8
#define SQUARED_BLOCK_DIM (BLOCK_DIM*BLOCK_DIM)
#define TRIPLE_SQUARED_BLOCK_DIM (3*SQUARED_BLOCK_DIM)

#ifdef USE_IPP
const Ipp16s Y_K1_QUANT_TBL[8][8] = { {16,11,10,16,24,40,51,61},
                             {12,12,14,19,26,58,60,55},
                             {14,13,16,24,40,57,69,56},
                             {14,17,22,29,51,87,80,62},
                             {18,22,37,56,68,109,103,77},
                             {24,35,55,64,81,104,113,92},
                             {49,64,78,87,103,121,120,101},
                             {72,92,95,98,112,100,103,99} };

const Ipp16s CBCR_K2_QUANT_TBL[8][8] = { {17,18,24,47,99,99,99,99},
                                {18,21,26,66,99,99,99,99},
                                {24,26,56,99,99,99,99,99},
                                {47,66,99,99,99,99,99,99},
                                {99,99,99,99,99,99,99,99},
                                {99,99,99,99,99,99,99,99},
                                {99,99,99,99,99,99,99,99},
                                {99,99,99,99,99,99,99,99} };
#else
const int Y_K1_QUANT_TBL[8][8] = { {16,11,10,16,24,40,51,61},
                             {12,12,14,19,26,58,60,55},
                             {14,13,16,24,40,57,69,56},
                             {14,17,22,29,51,87,80,62},
                             {18,22,37,56,68,109,103,77},
                             {24,35,55,64,81,104,113,92},
                             {49,64,78,87,103,121,120,101},
                             {72,92,95,98,112,100,103,99} };

const int CBCR_K2_QUANT_TBL[8][8] = { {17,18,24,47,99,99,99,99},
                                {18,21,26,66,99,99,99,99},
                                {24,26,56,99,99,99,99,99},
                                {47,66,99,99,99,99,99,99},
                                {99,99,99,99,99,99,99,99},
                                {99,99,99,99,99,99,99,99},
                                {99,99,99,99,99,99,99,99},
                                {99,99,99,99,99,99,99,99} };
#endif

#ifdef USE_IPP
void ipp_read_block(FILE* fil, long img_start_pos, Ipp8u* block_arr, int block_nr, const int NUM_BLOCKS_PER_AXIS) {
    int block_2d_row = block_nr / NUM_BLOCKS_PER_AXIS;
    int block_2d_col = block_nr - block_2d_row * NUM_BLOCKS_PER_AXIS;

    fseek(fil, (img_start_pos + block_2d_row * NUM_BLOCKS_PER_AXIS * 3 * BLOCK_DIM * BLOCK_DIM + block_2d_col * BLOCK_DIM * 3), SEEK_SET);
    for (int r = 0; r < BLOCK_DIM; r++) {
        for (int c = 0; c < BLOCK_DIM; c++) {
            fread(&block_arr[3 * (BLOCK_DIM * r + c)], sizeof(char), 3, fil);
        }
        fseek(fil, (NUM_BLOCKS_PER_AXIS - 1) * 3 * BLOCK_DIM, SEEK_CUR);
    }
}

void ipp_rgb2ycbcr(Ipp8u block_arr[TRIPLE_SQUARED_BLOCK_DIM], Ipp8u new_block_arr[3][SQUARED_BLOCK_DIM]) {
    Ipp8u* dst_arr[3];
    
    dst_arr[0] = new_block_arr[0];
    dst_arr[1] = new_block_arr[1];
    dst_arr[2] = new_block_arr[2];
    
    IppiSize roi_size = { BLOCK_DIM, BLOCK_DIM };
    ippiRGBToYUV_8u_C3P3R(block_arr, 3 * BLOCK_DIM, dst_arr, BLOCK_DIM, roi_size);
}


void ipp_calc_dct(Ipp8u block_arr[3][SQUARED_BLOCK_DIM], Ipp16s block_coefs_arr[3][SQUARED_BLOCK_DIM]) {
    IppiSize roi_size = { BLOCK_DIM, BLOCK_DIM };
    ippiDCT8x8FwdLS_8u16s_C1R(block_arr[0], BLOCK_DIM, block_coefs_arr[0], -128);
    ippiDCT8x8FwdLS_8u16s_C1R(block_arr[1], BLOCK_DIM, block_coefs_arr[1], -128);
    ippiDCT8x8FwdLS_8u16s_C1R(block_arr[2], BLOCK_DIM, block_coefs_arr[2], -128);
}

void ipp_quantize_dct_coefs(Ipp16s block_coefs_arr[3][SQUARED_BLOCK_DIM]) {
    for (int u = 0; u < BLOCK_DIM; u++) {
        for (int v = 0; v < BLOCK_DIM; v++) {
            block_coefs_arr[0][BLOCK_DIM * u + v] = round(block_coefs_arr[0][BLOCK_DIM * u + v] / Y_K1_QUANT_TBL[u][v]);
            block_coefs_arr[1][BLOCK_DIM * u + v] = round(block_coefs_arr[1][BLOCK_DIM * u + v] / CBCR_K2_QUANT_TBL[u][v]);
            block_coefs_arr[2][BLOCK_DIM * u + v] = round(block_coefs_arr[2][BLOCK_DIM * u + v] / CBCR_K2_QUANT_TBL[u][v]);
        }
    }
}

void ipp_print_results_to_file(FILE* out_fil, Ipp16s block_coefs_arr[3][SQUARED_BLOCK_DIM]) {
    for (int i = 0; i < BLOCK_DIM; i++) {
        fprintf(out_fil, "%d", (int)block_coefs_arr[0][BLOCK_DIM * i]);
        for (int j = 1; j < BLOCK_DIM - 1; j++) {
            fprintf(out_fil, " %d", (int)block_coefs_arr[0][BLOCK_DIM * i + j]);
        }
        fprintf(out_fil, " %d\n", (int)block_coefs_arr[0][BLOCK_DIM * i + BLOCK_DIM - 1]);
    }
    fprintf(out_fil, "\n");

    for (int i = 0; i < BLOCK_DIM; i++) {
        fprintf(out_fil, "%d", (int)block_coefs_arr[1][BLOCK_DIM * i]);
        for (int j = 1; j < BLOCK_DIM - 1; j++) {
            fprintf(out_fil, " %d", (int)block_coefs_arr[1][BLOCK_DIM * i + j]);
        }
        fprintf(out_fil, " %d\n", (int)block_coefs_arr[1][BLOCK_DIM * i + BLOCK_DIM - 1]);
    }
    fprintf(out_fil, "\n");

    for (int i = 0; i < BLOCK_DIM - 1; i++) {
        fprintf(out_fil, "%d", (int)block_coefs_arr[2][BLOCK_DIM * i]);
        for (int j = 1; j < BLOCK_DIM - 1; j++) {
            fprintf(out_fil, " %d", (int)block_coefs_arr[2][BLOCK_DIM * i + j]);
        }
        fprintf(out_fil, " %d\n", (int)block_coefs_arr[2][BLOCK_DIM * i + BLOCK_DIM - 1]);
    }

    fprintf(out_fil, "%d", (int)block_coefs_arr[2][BLOCK_DIM * (BLOCK_DIM - 1)]);
    for (int j = 1; j < BLOCK_DIM; j++) {
        fprintf(out_fil, " %d", (int)block_coefs_arr[2][BLOCK_DIM * (BLOCK_DIM - 1) + j]);
    }
}
#else
void read_block(FILE* fil, long img_start_pos, double block_arr[BLOCK_DIM][BLOCK_DIM][3], int block_nr, const int NUM_BLOCKS_PER_AXIS) {
    int block_2d_row = block_nr / NUM_BLOCKS_PER_AXIS;
    int block_2d_col = block_nr - block_2d_row * NUM_BLOCKS_PER_AXIS;

    fseek(fil, (img_start_pos + block_2d_row * NUM_BLOCKS_PER_AXIS * 3 * BLOCK_DIM * BLOCK_DIM + block_2d_col * BLOCK_DIM * 3), SEEK_SET);
    unsigned char r_cur, g_cur, b_cur;
    for (int r = 0; r < BLOCK_DIM; r++) {
        for (int c = 0; c < BLOCK_DIM; c++) {
            fread(&r_cur, sizeof(char), 1, fil);
            fread(&g_cur, sizeof(char), 1, fil);
            fread(&b_cur, sizeof(char), 1, fil);
            block_arr[r][c][0] = r_cur;
            block_arr[r][c][1] = g_cur;
            block_arr[r][c][2] = b_cur;
        }
        fseek(fil, (NUM_BLOCKS_PER_AXIS - 1) * 3 * BLOCK_DIM, SEEK_CUR);
    }
}

double* rgb2ycbcr(double block_pixel[3]) {
    double r_comp, g_comp, b_comp;
    r_comp = (block_pixel[0]);
    g_comp = (block_pixel[1]);
    b_comp = (block_pixel[2]);

    block_pixel[0] = 0.299 * r_comp + 0.587 * g_comp + 0.114 * b_comp - 128;
    block_pixel[1] = -0.1687 * r_comp - 0.3313 * g_comp + 0.5 * b_comp;
    block_pixel[2] = 0.5 * r_comp - 0.4187 * g_comp - 0.0813 * b_comp;

    return block_pixel;
}


void calc_dct(double block_arr[BLOCK_DIM][BLOCK_DIM][3], double block_coefs_arr[BLOCK_DIM][BLOCK_DIM][3], const double C_UV) {
    const double PI_OVER_TWO_BLOCK_DIM = M_PI / (2 * BLOCK_DIM);
    const double PREFIX_COEF = 2.0 / BLOCK_DIM;
    for (int u = 0; u < BLOCK_DIM; u++) {
        double c_u = (u == 0) ? C_UV : 1;
        for (int v = 0; v < BLOCK_DIM; v++) {
            double c_v = (v == 0) ? C_UV : 1;

            double uv_dct_coef[3];
            uv_dct_coef[0] = 0;
            uv_dct_coef[1] = 0;
            uv_dct_coef[2] = 0;

            for (int i = 0; i < BLOCK_DIM; i++) {
                for (int j = 0; j < BLOCK_DIM; j++) {
                    double cur_pix[3] = { block_arr[i][j][0], block_arr[i][j][1], block_arr[i][j][2] };
                    uv_dct_coef[0] += cur_pix[0] * cos((2 * i + 1) * u * PI_OVER_TWO_BLOCK_DIM) * cos((2 * j + 1) * v * PI_OVER_TWO_BLOCK_DIM);
                    uv_dct_coef[1] += cur_pix[1] * cos((2 * i + 1) * u * PI_OVER_TWO_BLOCK_DIM) * cos((2 * j + 1) * v * PI_OVER_TWO_BLOCK_DIM);
                    uv_dct_coef[2] += cur_pix[2] * cos((2 * i + 1) * u * PI_OVER_TWO_BLOCK_DIM) * cos((2 * j + 1) * v * PI_OVER_TWO_BLOCK_DIM);
                }
            }
            uv_dct_coef[0] = PREFIX_COEF * c_u * c_v * uv_dct_coef[0];
            uv_dct_coef[1] = PREFIX_COEF * c_u * c_v * uv_dct_coef[1];
            uv_dct_coef[2] = PREFIX_COEF * c_u * c_v * uv_dct_coef[2];
            block_coefs_arr[u][v][0] = uv_dct_coef[0];
            block_coefs_arr[u][v][1] = uv_dct_coef[1];
            block_coefs_arr[u][v][2] = uv_dct_coef[2];
        }
    }

}

void quantize_dct_coefs(double block_coefs_arr[BLOCK_DIM][BLOCK_DIM][3]) {
    for (int u = 0; u < BLOCK_DIM; u++) {
        for (int v = 0; v < BLOCK_DIM; v++) {
            block_coefs_arr[u][v][0] = round(block_coefs_arr[u][v][0] / Y_K1_QUANT_TBL[u][v]);
            block_coefs_arr[u][v][1] = round(block_coefs_arr[u][v][1] / CBCR_K2_QUANT_TBL[u][v]);
            block_coefs_arr[u][v][2] = round(block_coefs_arr[u][v][2] / CBCR_K2_QUANT_TBL[u][v]);
        }
    }
}

void print_results_to_file(FILE* out_fil, double block_coefs_arr[BLOCK_DIM][BLOCK_DIM][3]) {
    for (int i = 0; i < BLOCK_DIM; i++) {
        fprintf(out_fil, "%d", (int)block_coefs_arr[i][0][0]);
        for (int j = 1; j < BLOCK_DIM - 1; j++) {
            fprintf(out_fil, " %d", (int)block_coefs_arr[i][j][0]);
        }
        fprintf(out_fil, " %d\n", (int)block_coefs_arr[i][BLOCK_DIM - 1][0]);
    }
    fprintf(out_fil, "\n");

    for (int i = 0; i < BLOCK_DIM; i++) {
        fprintf(out_fil, "%d", (int)block_coefs_arr[i][0][1]);
        for (int j = 1; j < BLOCK_DIM - 1; j++) {
            fprintf(out_fil, " %d", (int)block_coefs_arr[i][j][1]);
        }
        fprintf(out_fil, " %d\n", (int)block_coefs_arr[i][BLOCK_DIM - 1][1]);
    }
    fprintf(out_fil, "\n");

    for (int i = 0; i < BLOCK_DIM - 1; i++) {
        fprintf(out_fil, "%d", (int)block_coefs_arr[i][0][2]);
        for (int j = 1; j < BLOCK_DIM - 1; j++) {
            fprintf(out_fil, " %d", (int)block_coefs_arr[i][j][2]);
        }
        fprintf(out_fil, " %d\n", (int)block_coefs_arr[i][BLOCK_DIM - 1][2]);
    }

    fprintf(out_fil, "%d", (int)block_coefs_arr[BLOCK_DIM - 1][0][2]);
    for (int j = 1; j < BLOCK_DIM; j++) {
        fprintf(out_fil, " %d", (int)block_coefs_arr[BLOCK_DIM - 1][j][2]);
    }
}
#endif



int main(int argc, char** argv) {

    FILE* fil = fopen(argv[1], "rb");
    FILE* out_fil = fopen(argv[2], "w");

    if (!fil) {
        perror("could not open file");
        return 1;
    }

    int ch;
    int newline_cnt = 0;
    while ((ch = fgetc(fil)) != EOF) {
        if (ch == '\n') {
            newline_cnt++;
            if (newline_cnt == 3) {
                break;
            }
        }
    }

    long img_start_pos = ftell(fil);

#ifdef USE_IPP
    Ipp8u block_arr[TRIPLE_SQUARED_BLOCK_DIM];
    Ipp8u new_block_arr[3][SQUARED_BLOCK_DIM];
    Ipp16s block_coefs_arr[3][SQUARED_BLOCK_DIM];
#else
    double block_arr[BLOCK_DIM][BLOCK_DIM][3];
    double block_coefs_arr[BLOCK_DIM][BLOCK_DIM][3];
#endif

    const int NUM_BLOCKS_PER_AXIS = IMG_DIM / BLOCK_DIM;
    const double C_UV = pow(0.5, 0.5);

    clock_t start_time = clock();
    for (int block_nr = 0; block_nr < NUM_BLOCKS_PER_AXIS* NUM_BLOCKS_PER_AXIS; block_nr++) {
#ifdef USE_IPP
        ipp_read_block(fil, img_start_pos, block_arr, block_nr, NUM_BLOCKS_PER_AXIS);

        // RGB -> YCbCr
        // in my_rgb struct now meaning of fields is:
        // r -> Y, g -> Cb, b -> Cr
        
        ipp_rgb2ycbcr(block_arr, new_block_arr);

        ipp_calc_dct(new_block_arr, block_coefs_arr);
        ipp_quantize_dct_coefs(block_coefs_arr);
#else
        read_block(fil, img_start_pos, block_arr, block_nr, NUM_BLOCKS_PER_AXIS);

        // RGB -> YCbCr
        // in my_rgb struct now meaning of fields is:
        // r -> Y, g -> Cb, b -> Cr
        for (int i = 0;i < BLOCK_DIM;i++) {
            for (int j = 0;j < BLOCK_DIM;j++) {
                // check that R G B are in range 0-255
                // if not, correct
                if (block_arr[i][j][0] < 0) {
                    block_arr[i][j][0] = 0;
                }
                else if (block_arr[i][j][0] > 255) {
                    block_arr[i][j][0] = 255;
                }

                if (block_arr[i][j][1] < 0) {
                    block_arr[i][j][1] = 0;
                }
                else if (block_arr[i][j][1] > 255) {
                    block_arr[i][j][1] = 255;
                }

                if (block_arr[i][j][2] < 0) {
                    block_arr[i][j][2] = 0;
                }
                else if (block_arr[i][j][2] > 255) {
                    block_arr[i][j][2] = 255;
                }


                // RGB -> YCbCr + translation for -128
                double* yuv_pixel = rgb2ycbcr(block_arr[i][j]);
                block_arr[i][j][0] = yuv_pixel[0];
                block_arr[i][j][1] = yuv_pixel[1];
                block_arr[i][j][2] = yuv_pixel[2];
            }
        }

        calc_dct(block_arr, block_coefs_arr, C_UV);
        quantize_dct_coefs(block_coefs_arr);
#endif
    }

#ifdef USE_IPP
    ipp_print_results_to_file(out_fil, block_coefs_arr);
#else
    print_results_to_file(out_fil, block_coefs_arr);
#endif

    fclose(fil);
    fclose(out_fil);
  
    clock_t end_time = clock();

    printf("CLOCK TIME = %f\n", (float)(end_time - start_time)/CLOCKS_PER_SEC);


    return 0;
}
